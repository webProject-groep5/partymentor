/* global ActiveXObject: true */

var loadXML = function () {
	var xmlhttp, xmlDoc, tags;

	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.open("GET", "fuiven.xml", false);
	xmlhttp.send();
	xmlDoc = xmlhttp.responseXML;
	tags = xmlDoc.getElementsByTagName("party");

	return tags;
};

var makePartyPanel = function (xmltag, inserPane) {
	var data = [];
	var panel, list, listItem, image;
	var i, j;

	if (xmltag === 'undefined') {
		return;
	}

	data[0] = xmltag.getElementsByTagName("image")[0].childNodes[0].nodeValue;
	data[1] = xmltag.getElementsByTagName("name")[0].childNodes[0].nodeValue;
	data[2] = xmltag.getElementsByTagName("description")[0].childNodes[0].nodeValue;
	data[3] = xmltag.getElementsByTagName("genre")[0].childNodes[0].nodeValue;
	data[4] = xmltag.getElementsByTagName("date")[0].childNodes[0].nodeValue;
	data[5] = xmltag.getElementsByTagName("organisator")[0].childNodes[0].nodeValue;

	panel = document.createElement("section");
	panel.id = "party-" + data[1];
	panel.setAttribute("class", "party-panel");

	image = document.createElement("img");
	image.src = data[0];
	panel.setAttribute("class", "banner-image");
	panel.appendChild(image);

	list = document.createElement("ul");
	panel.setAttribute("class", "party-info");

	for (i = 1, j = data.length; i < j; i++) {
		listItem = document.createElement("li");
		listItem.innerHTML = data[i];
		list.appendChild(listItem);
	}

	panel.appendChild(list);
	inserPane.appendChild(panel);
};

function main () {
	var calenderPane, xmlTags;
	var i, j;

	calenderPane = document.getElementById("calender-pane");
	calenderPane.innerHTML = "calender-pane<br>";
	xmlTags = loadXML();
	for (i = 0, j = xmlTags.length; i < j; i++) {
		makePartyPanel(xmlTags[i], calenderPane);
	}
}

main();
