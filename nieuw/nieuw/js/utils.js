var utils = {};

utils.toArray = function (obj) {
    var array = [], key;

    if (obj instanceof Object) {
        for (key in obj) {
            array.push(obj[key]);
        }
    }

    return array;
};

var DOM = {};

DOM.create = function (name, id, classe) {
    var element = document.createElement(name);
    element.id = id;
    element.setAttribute("class", classe);
    return element;
};

DOM.get = function (id) {
    return document.getElementById(id);
};

DOM.push = function (element, target) {
    target.appendChild(element);
};