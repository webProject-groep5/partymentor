var config = {
    "xml": "xml/fuiven.xml",
    "html": {
        "image": "image",
        "email": "email",
        "genre": "genre",
        "line-up": "line-up",
        "fuif-naam": "fuif-naam",
        "titel": "titel",
        "datum": "datum",
        "leeftijd": "leeftijd",
        "prijs": "prijs"
    }
};

var checkImgName = function (data) {
    var i, j;

    for (i = 0, j = data.length; i < j; i++) {
        if (data[i] === ".") {
            return true;
        }
    }
    return false;
};

