/* jshint
   browser: true,
   devel: true
   */
var fuifnaam = document.getElementById("fuifnaam"),
	organisator = document.getElementById("organisator"),
	slogan = document.getElementById("slogan"),
	catogerie = document.getElementById("categorie"),
	datum = document.getElementById("date"),
	leeftijd = document.getElementById("leeftijd"),
	genre = document.getElementById("genre"),
	lineup = document.getElementById("lineup"),
	inkom = document.getElementById("inkom"),
	mail = document.getElementById("mail"),
	straat = document.getElementById("straat"),
	huisnummer = document.getElementById("huis"),
	stad = document.getElementById("stad"),
	postcode = document.getElementById("post"),
	bericht = document.getElementById("bericht"),
	file = document.getElementById("file");

var lintfuifnaam = function () {
	if (fuifnaam.value === "") {
		alert("je fuifnaam is leeg vul deze in!");
		return;
	}
	if (!isNaN(fuifnaam.value)) {
		alert("je fuifnaam heeft geen Letters!");
	}
};

fuifnaam.onblur = function () {
	lintfuifnaam();
};

var lintorganisator = function () {
	if (organisator.value === "") {
		alert("een fuif kan je toch niet starten als je weet dat er geen organisator is?!");
	}
};

organisator.onblur = function () {
	lintorganisator();
};

var lintslogan = function () {
	if (slogan.value === "") {
		alert("een fuif zonder slogan? wat is dat voor een fuif?");
	}
};

slogan.onblur = function () {
	lintslogan();
};

var lintcatogerie = function () {
	if (catogerie.value === "") {
		alert("je fuif moet in een catogerie steken");
		return;
	}
	if (!isNaN(catogerie.value)) {
		alert("een catogerie heeft geen Letters!");
	}
};

catogerie.onblur = function () {
	lintcatogerie();
};

var lintdatum = function () {
	// http://www.the-art-of-web.com/javascript/validate-date/
	var allowBlank = true;
	var minYear = 1902;
	var maxYear = (new Date()).getFullYear();
	var re, regs = [];

	var errorMsg = "";

	re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;

	if (datum.value != '') {
		if (regs = datum.value.match(re)) {
			if (regs[1] < 1 || regs[1] > 31) {
				errorMsg = "Geen geldige dag: " + regs[1];
			} else if (regs[2] < 1 || regs[2] > 12) {
				errorMsg = "Geen geldige maand: " + regs[2];
			} else if (regs[3] < minYear || regs[3] > maxYear) {
				errorMsg = "Geen geldig jaar: " + regs[3] + " - moet tussen " + minYear + " en " + maxYear + " zijn.";
			}
		} else {
			errorMsg = "Foutieve datumnotatie: " + datum.value;
		}
	} else if (!allowBlank) {
		errorMsg = "Datum leeg!";
	}

	if (errorMsg != "") {
		alert(errorMsg);
		datum.focus();
		return false;
	}

	return true;
};

datum.onblur = function () {
	lintdatum();
};

var lintleeftijd = function () {
	if (leeftijd.value === "") {
		alert("Je leeftijd is leeg vul deze in.");
		return;
	}
	if (isNaN(leeftijd.value)) {
		alert("je leeftijd moet een getal zijn");
		return;
	}
	if (leeftijd.value <= 14 || leeftijd.value >= 100) {
		alert("de min. leeftijd moet tussen de 14 & 100 zijn");

	}
};

leeftijd.onblur = function () {
	lintleeftijd();
};

var lintgenre = function () {
	if (genre.value === "") {
		alert("je genre mag niet leeg zijn");
		return;
	}
	if (!isNaN(genre.value)) {
		alert("je genre mag geen getal hebben");
	}
};

genre.onblur = function () {
	lintgenre();
};

var lintlineup = function () {
	if (lineup.value === "") {
		alert("Een fuif zonder line-up is onmogelijk.");
	}
};

lineup.onblur = function () {
	lintlineup();
};

var lintinkom = function () {
	if (inkom.value === "") {
		alert("je inkom mag niet leeg zijn. als je het gratis wilt zet dan 0 in.");
		return;
	}
	if (isNaN(inkom.value)) {
		alert("je inkom moet een getal zijn");
		return;
	}
	if (inkom.value < 0) {
		alert("je inkom prijs kan niet onder nul gaan");

	}
};

inkom.onblur = function () {
	lintinkom();
};

var lintemail = function () {
	var i, j, at = false,
	dot = false;

	if (mail.value !== "") {
		for (i = 0, j = mail.value.length; i < j; i++) {
			if (mail.value[i] === "@") {
				at = true;
			}

			if (mail.value[i] === ".") {
				dot = true;
			}
		}
		return;
	}
	if (mail.value === "") {
		alert("je email mag niet leeg zijn");
		return;
	} else if (at === false) {
		alert("je hebt geen @ symbool in je email adres");
		return;
	} else if (dot === false) {
		alert("je hebt geen dot symbool in je email adres");
	}
};

mail.onblur = function () {
	lintemail();
};

var lintstraat = function () {
	if (straat.value === "") {
		alert("je straat is leeg dat mag niet.");
	}
	if (!isNaN(straat.value)) {
		alert("je straat heeft geen letters");
	}
};

straat.onblur = function () {
	lintstraat();
};

var linthuisnummer = function () {
	if (huisnummer.value === "") {
		alert("je huisnummer mag niet leeg zijn");
		return;
	}
	if (isNaN(huisnummer.value)) {
		alert("je huisnummer moet een getal zijn");
		return;
	}
	if (huisnummer.value <= 0) {
		alert("je huisnummer kan niet onder nul gaan of gelijk zijn aan 0");
	}
};

huisnummer.onblur = function () {
	linthuisnummer();
};

var lintstad = function () {
	if (stad.value === "") {
		alert("je stad is leeg dat mag niet.");
		return;
	}
	if (!isNaN(stad.value)) {
		alert("je stad heeft geen letters");
	}
};

stad.onblur = function () {
	lintstad();
};

var lintpostcode = function () {
	if (postcode.value === "") {
		alert("je postcode mag niet leeg zijn!");
		return;
	}
	if (isNaN(postcode.value)) {
		alert("je postcode moet een getal zijn");
		return;
	}
	if (postcode.value < 1000 || postcode.value > 9999) {
		alert("je postcode moet tussen de 1000 en 9999 zijn");
	}
};

postcode.onblur = function () {
	lintpostcode();
};

var lintbericht = function () {
	if (bericht.value === "") {
		alert("je bericht is leeg je kan dit niet versturen");
	}
};

bericht.onblur = function () {
	lintbericht();
};

var lintfile = function () {
	if (file.value === "") {
		alert("je moet een foto toevoegen");
	}

};

file.onblur = function () {
	lintfile();
};
