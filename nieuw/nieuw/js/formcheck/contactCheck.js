/* jshint
   browser: true,
   devel: true
   */
var naam = document.getElementById("naam"),
    bericht = document.getElementById("bericht"),
    email = document.getElementById("email"),
    voornaam = document.getElementById("voornaam");


var lintnaam = function () {
    if (naam.value === "") {
        alert("je naam is leeg vul deze in!");
    } else if (!isNaN(naam.value)) {
        alert("je naam heeft geen Letters!");
    } else if (naam.value === voornaam.value) {
        alert("je voornaam en naam mogen niet hetzelfde zijn");
    }
};

naam.onblur = function () {
    lintnaam();
};

var lintvoornaam = function () {
    if (voornaam.value === "") {
        alert("je voornaam is leeg vul deze in!");
    } else if (!isNaN(voornaam.value)) {
        alert("je voornaam heeft geen Letters!");
    } else if (voornaam.value === naam.value) {
        alert("je voornaam en naam mogen niet hetzelfde zijn");
    }
};

voornaam.onblur = function () {
    lintvoornaam();
};

var lintemail = function () {
    var i, j, at = false,
        dot = false;

    if (email.value !== "") {
        for (i = 0, j = email.value.length; i < j; i++) {
            if (email.value[i] === "@") {
                at = true;
            }

            if (email.value[i] === ".") {
                dot = true;
            }
        }
    }

    if (email.value === "") {
        alert("je email mag niet leeg zijn");
    } else if (at === false) {
        alert("je hebt geen @ symbool in je email adres");
    } else if (dot === false) {
        alert("je hebt geen dot symbool in je email adres");
    }
};

email.onblur = function () {
    lintemail();
};

var lintbericht = function () {
    if (bericht.value === "") {
        alert("je bericht is leeg je kan dit niet versturen");
    }
};

bericht.onblur = function () {
    lintbericht();
};