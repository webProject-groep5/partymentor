/* global ActiveXObject: true */
var config = {
    "xml": {
        "fileFuiven": "xml/fuiven.xml",
        "xmlHead": "party",
        "xmlBody": {
            "image": "banner",
            "name": "naam",
            "date": "datum",
            "description": "slogan",
            "genre": "genre",
            "organisator": "organisator",
            "email": "email",
            "categorie": "categorie",
            "prise": "prijs",
            "lineup": "lineup"
        },
        "imagePath": "img/fuifbanner/"
    },
    "panelId": "party",
    "panelClass": "party-panel",
    "imageClass": "party-banner",
    "listClass": "party-info",
    "fuifpage": "fuif.html"
};

var loadXML = function (xmlfile) {
    var xmlhttp, xmlDoc, tags;

    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        alert("You use IE6 or lower, please upgrade (it's free)");
        throw "IE6 found, kill it!";
        // activeX object for IE6 or <
        // temp comment for no syntax error on this
        // xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.open("GET", xmlfile, false);
    xmlhttp.send();
    xmlDoc = xmlhttp.responseXML;
    tags = xmlDoc.getElementsByTagName("party");

    return tags;
};

var makePartyPanel = function (xmltag, inserPane) {
    var data = {};
    var panel, list, listItem, image, imglink;
    var key;

    if (xmltag === 'undefined') {
        return;
    }

    var i, j, keys = utils.toArray(config.xml.xmlBody);

    for (i = 0, j = keys.length; i < j; i++) {
        data[keys[i]] = xmltag.getElementsByTagName(keys[i])[0].childNodes[0].nodeValue;
    }

    //{{{
    //data[config.xml.xmlBody.image] = xmltag.getElementsByTagName(config.xml.xmlBody.image)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.name] = xmltag.getElementsByTagName(config.xml.xmlBody.name)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.description] = xmltag.getElementsByTagName(config.xml.xmlBody.description)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.genre] = xmltag.getElementsByTagName(config.xml.xmlBody.genre)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.lineup] = xmltag.getElementsByTagName(config.xml.xmlBody.lineup)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.date] = xmltag.getElementsByTagName(config.xml.xmlBody.date)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.organisator] = xmltag.getElementsByTagName(config.xml.xmlBody.organisator)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.email] = xmltag.getElementsByTagName(config.xml.xmlBody.email)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.prise] = xmltag.getElementsByTagName(config.xml.xmlBody.prise)[0].childNodes[0].nodeValue;
    //}}}

    panel = document.createElement("section");
    panel.id = config.panelId + "-" + config.xml.xmlBody.name;
    panel.setAttribute("class", config.panelClass);

    image = document.createElement("img");
    imglink = document.createElement("a");
    imglink.setAttribute("href", config.fuifpage);

    imglink.onclick = function () {
        localStorage.setItem("fuifIndex", xmltag.getAttribute("index"));
    };

    image.src = config.xml.imagePath + data[config.xml.xmlBody.image];
    image.setAttribute("class", config.imageClass);
    imglink.appendChild(image);
    panel.appendChild(imglink);

    list = document.createElement("ul");
    list.setAttribute("class", config.listClass);

    for (key in data) {
        if (key != config.xml.xmlBody.image) {
            listItem = document.createElement("li");
            if (key == config.xml.xmlBody.prise) {
                listItem.innerHTML = "<strong>" + key + ":</strong> " + Math.round(Number(data[key]) * 100) / 100;
            } else {
                listItem.innerHTML = "<strong>" + key + ":</strong> " + data[key];
            }
            list.appendChild(listItem);
        }
    }

    panel.appendChild(list);
    inserPane.appendChild(panel);
};

function main() {
    var calenderPane, xmlTags;
    var i, j;
    var titel = document.createElement('h1');
    titel.innerHTML = "Recente fuiven";
    titel.style.textAlign = 'center';
    titel.style.fontSize = '24px';
    titel.style.textDecoration = 'underline';
    titel.style.marginBottom = '1em';


    calenderPane = document.getElementById("calender-pane");
    document.getElementById("calender-pane").appendChild(titel);
    xmlTags = loadXML(config.xml.fileFuiven);
    for (i = 0, j = xmlTags.length; i < j; i++) {
        makePartyPanel(xmlTags[i], calenderPane);
    }
}

window.onload = main;
