/* global ActiveXObject: true */
var config = { // {{{1
    "xml": {
        "fileFuiven": "xml/fuiven.xml",
        "xmlHead": "party",
        "xmlBody": {
            "image": "banner",
            "name": "naam",
            "date": "datum",
            "description": "beschrijving",
            "minage": "leeftijd",
            "genre": "genre",
            "categorie": "categorie",
            "organisator": "organisator",
            "email": "email",
            "prise": "prijs",
            "lineup": "lineup",
            "locatie": "locatie"
        },
        "imagePath": "img/fuifbanner/"
    },
    "htmlPanel": "content",
    "imageClass": "party-banner-large",
    "listClass": "party-info",
    "fuifpage": "fuif.html"
};

var loadXML = function (xmlfile) { // {{{1
    var xmlhttp, xmlDoc, tags;

    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        alert("You use IE6 or lower, please upgrade (it's free)");
        throw "IE6 found, kill it!";
        //activeX object for IE6 or <
        //temp comment for no syntax error on this
        //xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.open("GET", xmlfile, false);
    xmlhttp.send();
    xmlDoc = xmlhttp.responseXML;
    tags = xmlDoc.getElementsByTagName("party");

    return tags;
};

var insertInfo = function (xmltag) { // {{{1
    var data = {};
    var para, image, title, panel, frame;
    var key;

    if (xmltag === 'undefined') {
        return;
    }

    var i, j, keys = utils.toArray(config.xml.xmlBody);

    for (i = 0, j = keys.length; i < j; i++) {
        data[keys[i]] = xmltag.getElementsByTagName(keys[i])[0].childNodes[0].nodeValue;
    }

    // {{{
    //data[config.xml.xmlBody.image] = xmltag.getElementsByTagName(config.xml.xmlBody.image)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.name] = xmltag.getElementsByTagName(config.xml.xmlBody.name)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.description] = xmltag.getElementsByTagName(config.xml.xmlBody.description)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.genre] = xmltag.getElementsByTagName(config.xml.xmlBody.genre)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.lineup] = xmltag.getElementsByTagName(config.xml.xmlBody.lineup)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.date] = xmltag.getElementsByTagName(config.xml.xmlBody.date)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.organisator] = xmltag.getElementsByTagName(config.xml.xmlBody.organisator)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.email] = xmltag.getElementsByTagName(config.xml.xmlBody.email)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.categorie] = xmltag.getElementsByTagName(config.xml.xmlBody.categorie)[0].childNodes[0].nodeValue;
    //data[config.xml.xmlBody.prise] = xmltag.getElementsByTagName(config.xml.xmlBody.prise)[0].childNodes[0].nodeValue;
    // }}}

    var url, adres = {};
    adres.straat = xmltag.getElementsByTagName(config.xml.xmlBody.locatie)[0].children[0].innerHTML;
    adres.nummer = xmltag.getElementsByTagName(config.xml.xmlBody.locatie)[0].children[1].innerHTML;
    adres.stad = xmltag.getElementsByTagName(config.xml.xmlBody.locatie)[0].children[2].innerHTML;

    url = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d160410.2416827943!2d3.714485049999996!3d51.08244005000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c3b79106d7a997%3A0x176aba32c5edfa4a!2s" + adres.straat + "+" + adres.nummer + "%2C+" + adres.stad + "!5e0!3m2!1snl!2sbe!4v1430206047301";

    //data[config.xml.xmlBody.locatie] = xmltag.getElementsByTagName(config.xml.xmlBody.locatie)[0].children[0].innerHTML;
    data[config.xml.xmlBody.locatie] = url;

    panel = document.getElementById(config.htmlPanel);

    document.getElementsByTagName("title")[0].innerHTML = "Fuif | " + data[config.xml.xmlBody.name];
    image = document.createElement("img");

    image.src = config.xml.imagePath + data[config.xml.xmlBody.image];
    image.setAttribute("class", config.imageClass);

    frame = document.createElement("iframe");
    frame.src = data[config.xml.xmlBody.locatie];
    frame.width = "600px";
    frame.height = "400px";

    panel.appendChild(image);

    for (key in data) {
        if (key != config.xml.xmlBody.image && key != config.xml.xmlBody.locatie) {
            title = document.createElement("h2");
            title.innerHTML = key + ":";
            para = document.createElement("p");
            para.innerHTML = data[key];
            panel.appendChild(title);
            panel.appendChild(para);
        }
    }
    panel.appendChild(frame);
};

function main() { // {{{1
    var xmltags;

    xmltags = loadXML(config.xml.fileFuiven);
    console.log(xmltags[0]);
    insertInfo(xmltags[Number(localStorage.getItem("fuifIndex"))]);
}

window.onload = main;
