var hamburger = document.getElementById("small-nav");
var nav = document.getElementById("groot-nav");
var size = document;
var open = false;
var drop = document.getElementById("catdown");
var down = false;

hamburger.onclick = function () {
    if (open === false) {
        nav.style.display = 'block';
        open = true;
    }
    else {
        nav.style.display = 'none';
        open = false;
    }
}

window.onresize = function (event) {
    if (window.innerWidth > 401) {
        nav.style.display = 'inline-block';
        open = true;
    } else {
        nav.style.display = 'none';
        open = false;
        drop.style.display = 'none';
        down = false;
    }
};

var cat = document.getElementById("cat");

cat.onclick = function (e) {
    e.preventDefault();
    if (down === false) {
        drop.style.display = 'block';
        down = true;
    } else {
        drop.style.display = 'none';
        down = false;
    }
}

cat.onmouseover = function () {
    if (window.innerWidth <= 401) {
        if (down === false) {
            drop.style.display = 'block';
            down = true;
        } else {
            drop.style.display = 'none';
            down = false;
        }

    }
}